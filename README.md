# Bitbucket Pipelines sample project #

### What is this repository for? ###

* This is a sample project in order to check the Bitbucket Pipelines features.
* Version: 1.0

### How to run the project? ###

`./gradlew run`

### How to test the project? ###

`./gradlew test` or `./gradlew check`